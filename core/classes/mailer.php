<?php

/**
 * Description of mailer
 *
 * * @author Valentin Balt <valentin.balt@gmail.com>
 */
class Mailer extends Config {

    const Q_MAIL = 'mail';

    public function __construct() {
        parent::__construct();

        $this->logger = new Logger(get_class());
    }

    public function getSwiftMailer($noneuropean = nul) {
        return $this->configureSwift($this->getConfig('transport'), $noneuropean);
    }

    private function configureSwift($transportName = 'mail', $noneuropean = null) {
        if (!class_exists('Swift')) {
            if ((@include 'swift_required.php') === false) {
                require 'Swift/swift_required.php';
            }
        }

        $opt = $this->getConfig('options');

        if ($transportName == 'smtp') {

            if ($noneuropean == "noneu") {
                $transport = (new Swift_SmtpTransport($opt['host'], $opt['port']))
                        ->setUsername($opt['username_eu'])
                        ->setPassword($opt['password_eu'])
                        ->setEncryption($opt['encryption']);
            } else {
                $transport = (new Swift_SmtpTransport($opt['host'], $opt['port']))
                        ->setUsername($opt['username'])
                        ->setPassword($opt['password'])
                        ->setEncryption($opt['encryption']);
            }
        } else {
            $transport = new Swift_SendmailTransport();
        }

        return new Swift_Mailer($transport);
    }

    public function send($address, $subject, $text, $noneuropean = null, $from = null, $text_only = false, $attachments = null, $lang = null) {
        if (!$from) {
            $from = $this->getConfig('from');
        }

        if ($noneuropean == "noneu")
            $from = $this->getConfig('from_eu');
        else
            $from = $this->getConfig('from');
        $to = $address;

        if ($noneuropean == "noneu")
            $header = $this->getVar('emails', 'header_noneu', $lang);
        else
            $header = $this->getVar('emails', 'header', $lang);

        if ($noneuropean == "noneu") {
            $footer = $this->getVar('emails', 'footer_noneu', $lang);
        } else if ($noneuropean == "eu") {
            $footer = $this->getVar('emails', 'footer', $lang);
        } else if (empty($noneuropean)) {
            $footer = $this->getVar('emails', 'footer_common', $lang);
        }
        $footer = str_replace('###WEBSITE_LINK###', BRAND_WEBSITE, $footer);
        $footer = str_replace('###INSTAGRAM_LINK###', INSTAGRAM_LINK, $footer);
        $footer = str_replace('###FACEBOOK_LINK###', FACEBOOK_LINK, $footer);
        $footer = str_replace('###YOUTUBE_LINK###', YOUTUBE_LINK, $footer);
        $messageHTML = $header . ($text) . $footer;

        try {
            $mailer = $this->getSwiftMailer($noneuropean);
            $message = (new Swift_Message($subject))
                    // Set To
                    ->setTo($to)
                    // Set From message
                    ->setFrom($from);

            if ($bcc = $this->getConfig('bcc')) {
                $message->setBcc($bcc);
            }
            if (!$text_only) {
                // Add images
                if ($this->getConfig('images')) {
                    foreach ($this->getConfig('images') as $file => $info) {
                        if (stristr($messageHTML, '"' . $info['src'] . '"') !== false) {
                            $messageHTML = str_replace(
                                    '"' . $info['src'] . '"', '"' . $message->embed(Swift_Image::fromPath(PATH_WWW . $file)) . '"', $messageHTML
                            );
                        }
                    }
                }
                $message
                        ->setBody($messageHTML, 'text/html')
                        ->addPart(strip_tags($text));
            } else {
                $message->setBody($text);
            }

            if (!empty($attachments)) {
                $zipped = [];
                foreach ($attachments as $adata) {
                    // check for "zip" requirements
                    if (!empty($adata['zip'])) {
                        $zipped[] = $adata;
                    } else {
                        $attachment = (new Swift_Attachment())
                                ->setFilename($adata['name'])
                                ->setContentType($adata['type'])
                                ->setBody($adata['data']);
                        $message->attach($attachment);
                    }
                }

                if (!empty($zipped)) {
                    foreach ($zipped as $k => $adata) {
                        $zip = new ZipArchive();
                        $fn = tempnam(sys_get_temp_dir(), uniqid());
                        if ($zip->open($fn, ZipArchive::CREATE) === TRUE) {
                            $zip->addFromString($adata['name'], $adata['data']);
                            $zip->close();
                            $attachment = (new Swift_Attachment())
                                    ->setFilename($adata['name'] . '.zip')
                                    ->setContentType('application/zip')
                                    ->setBody(file_get_contents($fn));
                            $message->attach($attachment);
                        }
                        unlink($fn);
                    }
                }
            }
            $ret = $mailer->send($message);

            $this->logger->debug('Sent email noneuropean' . $noneuropean . ' from ' . $from . ' to: ' . $to . ': ' . (!$ret ? 'Error' : 'OK'));
        } catch (\Exception $e) {
            $this->logger->error('Mailer error: ' . $e->getMessage());
        }
    }

    public function queueRun($task) {
        $i = unserialize($task['params']);

        $this->send(
                $i['email_address'], $i['email_subject'], $i['email_html'], !empty($i['noneuropean']) ? $i['noneuropean'] : '', empty($i['email_from']) ? null : $i['email_from'], !empty($i['text_only']), empty($i['email_attachments']) ? null : $i['email_attachments'], !empty($i['lang']) ? $i['lang'] : ''
        );

        return true;
    }

}
